package coursework.posts;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import coursework.threads.Threads;
import coursework.userProfile.UserProfile;

@SuppressWarnings("serial")
@Entity
@Table(name = "posts")
public class Posts implements java.io.Serializable {
	
	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE)
	private Long id;
	
	@ManyToOne(targetEntity = UserProfile.class)
	@JoinColumn(name="userProfile_username", referencedColumnName = "username")
	private UserProfile userProfile; // foreign key
	
	@ManyToOne(targetEntity = Threads.class)
	@JoinColumn(name="thread_id")
	private Threads thread; //foreign key
	
	@NotBlank(message = Posts.NOT_BLANK_MESSAGE)
	@Size(max = 255)
	private String message;
	
	private Instant created;
	
	protected Posts() {
	}
	
	public Posts(String message) {
		this.created = Instant.now();
	}
	
	public Long getId() { return this.id;}
	public String getMessage() { return this.message; }
	public Instant getCreated() { return this.created; }
	public UserProfile getUserProfile() { return this.userProfile; }
	public Threads getThread() { return this.thread; }

	public void setMessage(String message) { this.message = message; }
	public void setUserProfile(UserProfile userProfile) { this.userProfile = userProfile; }
	public void setThread(Threads thread) { this.thread = thread; }

	
}

package coursework.posts;

import coursework.account.Account;
import coursework.account.AccountService;
import coursework.threads.ThreadsRepository;
import coursework.userProfile.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class PostsController {

	@Autowired
	private PostsRepository postsRepository;

	@Autowired
	private ThreadsRepository threadsRepository;

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private PostsService postsService;

	@Autowired
	private AccountService accService;

	private static final int MAX_MESSAGE_LENGTH = 255;

	@ModelAttribute(value = "module")
	String module() {
		return "posts";
	}

	/**
	 * Create new post
	 */
	@GetMapping(value = "/thread/{id}/posts/new")
	public String newPost(@PathVariable Long id, Model model) {
		model.addAttribute("post", new Posts());
		model.addAttribute("thread_id", id);
		model.addAttribute("thread", threadsRepository.findOne(id));
		return "posts/createPost";
	}

	/**
	 * Save new post
	 */
	@PostMapping(value = "/thread/{thread_id}/posts/new", params= { "action=success" })
	public String saveNewPost(@PathVariable Long thread_id, @Valid Posts post, Errors errors, Model model,
			Principal principal) {
		if (errors.hasErrors()) {
			model.addAttribute("post", post);
			model.addAttribute("thread_id", thread_id);
			model.addAttribute("thread", threadsRepository.findOne(thread_id));
			return "posts/createPost";
		}

		post.setThread(threadsRepository.findOne(thread_id));
		post.setUserProfile(accService.getCurrentAccount(principal.getName()).getUserProfile());

		int numberOfPosts = post.getUserProfile().getNumberOfPosts() + 1;
		post.getUserProfile().setNumberOfPosts(numberOfPosts);

		userProfileService.updateNumberOfPosts(accService.getCurrentAccount(principal.getName()).getId(),
				numberOfPosts);
		postsService.savePost(post);

		return "redirect:/thread/get?id=" + thread_id;
	}
	
	/**
	 * Redirect back to form (validation errors)
	 */
	@PostMapping(value = "/thread/{thread_id}/posts/new", params= { "action=warning" })
	public String returnSaveNewPost(@PathVariable Long thread_id, Posts post, Errors errors, Model model,
			Principal principal) {
		model.addAttribute("post", post);
		model.addAttribute("thread_id", thread_id);
		model.addAttribute("thread", threadsRepository.findOne(thread_id));
		
		if(post.getMessage().length() > this.MAX_MESSAGE_LENGTH)
			model.addAttribute("message","form.message.toolong");
		else if(post.getMessage().length() == 0)
			model.addAttribute("message","form.message.blank");
		
		model.addAttribute("alertClass", "warning");
		
		return "posts/createPost";
	}

	/**
	 * Get single post
	 */
	@GetMapping(value = "/thread/{thread_id}/post/get", params = { "id" })
	public String getPost(@PathVariable Long thread_id, @RequestParam Long id, Model model, Principal principal) {
		Account account = accService.getCurrentAccount(principal.getName());
		model.addAttribute("username", account.getUsername());
		model.addAttribute("userRole", account.getRole());
		model.addAttribute("thread", threadsRepository.findOne(thread_id));
		model.addAttribute("post", postsRepository.findOne(id));
		return "posts/post";
	}
	
	/**
	 * Edit a post
	 */
	@GetMapping(value = "/thread/{thread_id}/post/edit", params = { "id" })
	public String editPostOfThread(@PathVariable Long thread_id, @RequestParam Long id, Model model,
			Principal principal) {
		Account account = accService.getCurrentAccount(principal.getName());
		model.addAttribute("username", account.getUsername());
		model.addAttribute("thread", threadsRepository.findOne(thread_id));
		model.addAttribute("post", postsRepository.findOne(id));
		return "posts/editPost";
	}

	/**
	 * Update post
	 */
	@PostMapping(value = "/thread/{thread_id}/post/edit", params = { "id", "action=success" })
	public String updatePost(@PathVariable Long thread_id, @RequestParam Long id, @Valid Posts post, Errors errors,
			Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("post", post);
			model.addAttribute("thread", threadsRepository.findOne(thread_id));
			return "posts/editPost";
		}
		postsRepository.updateMessage(post.getMessage(), id);
		return "redirect:/thread/" + thread_id + "/post/get?id=" + id;
	}

	/**
	 * Redirect back to form (validation errors)
	 */
	@PostMapping(value = "/thread/{thread_id}/post/edit", params = { "id", "action=warning" })
	public String returnUpdatePost(@PathVariable Long thread_id, @RequestParam Long id, Posts tempPost, Model model,
			Principal principal) {
		Account account = accService.getCurrentAccount(principal.getName());
		Posts post = postsRepository.findOne(id);
		post.setMessage(tempPost.getMessage());
		
		model.addAttribute("username", account.getUsername());
		model.addAttribute("thread", threadsRepository.findOne(thread_id));		
		model.addAttribute("post", post);

		if (post.getMessage().length() > this.MAX_MESSAGE_LENGTH)
			model.addAttribute("message", "form.message.toolong");
		else if (post.getMessage().length() == 0)
			model.addAttribute("message", "form.message.blank");

		model.addAttribute("alertClass", "warning");

		return "posts/editPost";
	}

	/**
	 * Delete a post
	 */
	@GetMapping(value = "/post/delete", params = { "id" })
	public String deletePost(@RequestParam Long id) {
		Posts post = postsRepository.findOne(id);
		int numberOfPosts = post.getUserProfile().getNumberOfPosts() - 1;

		userProfileService.updateNumberOfPosts(post.getUserProfile().getAccount().getId(), numberOfPosts);

		postsRepository.delete(id);
		return "redirect:/threads";
	}
}
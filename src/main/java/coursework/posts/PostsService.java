package coursework.posts;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coursework.account.AccountRepository;
import coursework.threads.ThreadsRepository;

@Service
public class PostsService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private PostsRepository postsRepository;

	@Autowired
	private ThreadsRepository threadsRepository;

	@Autowired
	private AccountRepository accRepository;

	@PostConstruct
	protected void initialize() {
//		Posts postAdmin1 = new Posts("test message one", "test - admin");
//		Posts postAdmin2 = new Posts("test message two", "test - admin");
//		Posts postUser1 = new Posts("test message three", "test - user");
//		Posts postUser2 = new Posts("test message four", "test - user");
//
//		List<String> tags = new ArrayList<String>() {
//			{
//				add("Post Tag1");
//				add("Post Tag2");
//			}
//		};
//		Account admin = accountRepository.findOne((long) 1);
//		Account user = accountRepository.findOne((long) 2);
//		
//		Threads adminThread = threadsRepository.findOne((long) 2);
//		Threads userThread = threadsRepository.findOne((long) 3);
//
//		adminThread.setUserProfile(admin.getUserProfile());
//		userThread.setUserProfile(user.getUserProfile());
//
//		postAdmin1.setUserProfile(admin.getUserProfile());
//		postAdmin1.setThread(adminThread);
//		postAdmin2.setUserProfile(user.getUserProfile());
//		postAdmin2.setThread(userThread);
//		
//		postUser1.setUserProfile(admin.getUserProfile());
//		postUser1.setThread(adminThread);
//		postUser2.setUserProfile(user.getUserProfile());
//		postUser1.setThread(userThread);
//		
//		savePost(postAdmin1);
//		savePost(postAdmin2);
//		savePost(postUser1);
//		savePost(postUser2);
	}

	public Posts getById(Long id) {
		return this.postsRepository.findOne(id);
	}

	public Iterable<Posts> getAll() {
		return this.postsRepository.findAll();
	}

	public Posts savePost(Posts post) {
		this.postsRepository.save(post);
		return post;
	}

}

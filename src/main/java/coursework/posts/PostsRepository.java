package coursework.posts;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PostsRepository extends CrudRepository<Posts, Long> {

	@Transactional
	@Modifying
	@Query("UPDATE Posts SET message = :message WHERE id = :id")
	void updateMessage(@Param("message") String message, @Param("id") Long id);

	List<Posts> findByThreadId(Long threadId);
}

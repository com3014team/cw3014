package coursework.signup;

import coursework.account.Account;
import coursework.image.Image;
import coursework.userProfile.UserProfile;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.io.File;
import java.io.FileInputStream;

public class SignupForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
	private static final String EMAIL_MESSAGE = "{email.message}";
	private static final String EMAIL_EXISTS_MESSAGE = "{email-exists.message}";

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	@Email(message = SignupForm.EMAIL_MESSAGE)
	@EmailExists(message = SignupForm.EMAIL_EXISTS_MESSAGE)
	private String email;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String password;

	@NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String username;

	public Account createAccount() {
		File file = new File(System.getProperty("user.dir") + "/src/main/webapp/resources/images/defaultAvatar.jpg");
		byte[] bFile = new byte[ (int) file.length()];
		try {
			FileInputStream fip = new FileInputStream(file);
			fip.read(bFile);
			fip.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Image adminImg = new Image("defaultAvatar", "jpg", bFile);

		UserProfile userProfile = new UserProfile(getUsername());
		userProfile.setImg(adminImg);
		adminImg.setUserProfile(userProfile);
		Account account = new Account(getEmail(), getPassword(), "ROLE_USER");
		account.setUserProfile(userProfile);
		userProfile.setAccount(account);
		return account;
	}
	
	public void setEmail(String email) { this.email = email; }
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }

	public String getEmail() { return email; }
	public String getUsername() { return username; }
	public String getPassword() { return password; }
}

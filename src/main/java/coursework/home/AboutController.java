package coursework.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class AboutController {

	@ModelAttribute(value = "module")
	String module() {
		return "about";
	}

	@GetMapping(value = "/about")
	String about() {
		return "home/about";
	}
}

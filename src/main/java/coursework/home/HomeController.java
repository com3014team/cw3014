package coursework.home;

import coursework.account.Account;
import coursework.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;

@Controller
class HomeController {

	@Autowired
	AccountService accountService;

	@ModelAttribute("module")
	String module() {
		return "home";
	}

	@GetMapping("/")
	String index(Principal principal, Model model) {
		model.addAttribute("springVersion", SpringVersion.getVersion());
		if (principal != null) {
			Account acc = accountService.getCurrentAccount(principal.getName());
			model.addAttribute("username", acc.getUserProfile().getUsername());
		}
		return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
	}
	
}

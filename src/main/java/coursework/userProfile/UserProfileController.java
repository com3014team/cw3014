package coursework.userProfile;

import java.security.Principal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import coursework.account.Account;
import coursework.account.AccountService;

@Controller
public class UserProfileController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private UserProfileService userProfileService;

	@ModelAttribute(value = "module")
	String module() {
		return "profile";
	}

	/**
	 * Get a user profile
	 */
	@GetMapping(value = "/userProfile")
	public String userProfile(Model model, Principal principal) {
		Account acc = accountService.getCurrentAccount(principal.getName());
		Date accCreated = Date.from(acc.getCreated());
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");

		model.addAttribute("username", acc.getUserProfile().getUsername());
		model.addAttribute("userRole", acc.getRole());
		model.addAttribute("email", acc.getEmail());
		model.addAttribute("created", formatter.format(accCreated));
		model.addAttribute("numberOfPosts", acc.getUserProfile().getNumberOfPosts());
		model.addAttribute("numberOfThreads", acc.getUserProfile().getNumberOfThreads());
		model.addAttribute("image", Base64.getEncoder().encode(acc.getUserProfile().getImg()));

		return "userProfile/userProfile";
	}

	/**
	 * Get a user profile (after username update)
	 */
	@GetMapping(value = "/userProfile", params = { "action=success" })
	public String userProfileUpdate(Model model, Principal principal) {
		Account acc = accountService.getCurrentAccount(principal.getName());
		Date accCreated = Date.from(acc.getCreated());
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");

		model.addAttribute("username", acc.getUserProfile().getUsername());
		model.addAttribute("userRole", acc.getRole());
		model.addAttribute("email", acc.getEmail());
		model.addAttribute("created", formatter.format(accCreated));
		model.addAttribute("numberOfPosts", acc.getUserProfile().getNumberOfPosts());
		model.addAttribute("numberOfThreads", acc.getUserProfile().getNumberOfThreads());
		model.addAttribute("image", Base64.getEncoder().encode(acc.getUserProfile().getImg()));

		model.addAttribute("message", MessageFormat.format("Username changed to {0}!", acc.getUsername()));
		model.addAttribute("alertClass", "success");

		return "userProfile/userProfile";
	}

	/**
	 * Edit a user profile
	 */
	@GetMapping(value = "/userProfile/edit")
	public String editUserProfile(Model model, Principal principal) {
		Account account = accountService.getCurrentAccount(principal.getName());
		model.addAttribute("userProfile", account.getUserProfile());

		return "userProfile/editUserProfile";
	}

	/**
	 * Update a user profile
	 */
	@PostMapping(value = "/userProfile/get", params = { "id", "action=success" })
	public String updateUserProfile(@RequestParam Long id, UserProfile tempUserProfile, Principal principal) {
		Account acc = accountService.getCurrentAccount(principal.getName());
		userProfileService.updateUsername(acc.getId(), tempUserProfile.getUsername());

		return "redirect:/userProfile?action=success";
	}

	/**
	 * Redirect back to form (validation errors)
	 */
	@PostMapping(value = "/userProfile/get", params = { "id", "action=warning" })
	public String returnUserProfile(@RequestParam Long id, UserProfile tempUserProfile, Model model,
			Principal principal) {
		if (tempUserProfile.getUsername().length() > 50)
			model.addAttribute("message", "form.username.toolong");
		else
			model.addAttribute("message", "form.username.blank");
		model.addAttribute("alertClass", "warning");
		model.addAttribute("userProfile", accountService.getCurrentAccount(principal.getName()));

		return "userProfile/editUserProfile";
	}

	@GetMapping(value = "/upload")
	public String getUploadForm(Principal principal, Model model) {
		Account acc = accountService.getCurrentAccount(principal.getName());
		model.addAttribute("userProfile", acc.getUserProfile());
		return "userProfile/uploadForm";
	}

	@CrossOrigin
	@PostMapping(value = "/doUpload")
	public String uploadFile(@RequestParam("file") MultipartFile file, Principal principal,
			RedirectAttributes redirectAttributes) {
		
		if (file.isEmpty()) {
			System.out.println("FILE IS EMPTY========================================");
			redirectAttributes.addFlashAttribute("Message", "Please select a file to upload");
			return "redirect:/userProfile";
		}

		try {
			byte[] bytes = file.getBytes();
			accountService.getCurrentAccount(principal.getName()).getUserProfile().setImageBytes(bytes);
			
		} catch (Exception e) {

		}

		return "redirect:/userProfile";
	}

//	@PostMapping(value = "/doUpload")
//	@PostMapping(value = "/doUpload", headers={"content-type=multipart/form-data"})
//	public String handleFileUpload(@RequestParam("filename") CommonsMultipartFile file, Principal principal) {
//		Account acc = accountService.getCurrentAccount(principal.getName());
//		model.addAttribute("userProfile" , acc.getUserProfile());

//		storageService.store(file);
//		redirectAttributes.addFlashAttribute("message",
//				"You successfully uploaded " + file.getOriginalFilename() + "!");

//		return "redirect:/";
//	}
//
//	@PostMapping("/doUpload")
//	public String uploadFile(@RequestParam("file") MultipartFile file) {
////		String fileName = fileStorageService.storeFile(file);
//
////		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
////				.path("/downloadFile/")
////				.path(fileName)
////				.toUriString();
//
//		return "redirect:/userProfile";
//	}

}

package coursework.userProfile;

import coursework.account.Account;
import coursework.image.Image;
import coursework.posts.Posts;
import coursework.threads.Threads;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "userProfile")
public class UserProfile implements java.io.Serializable  {

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE)
	private Long id;

	@Column(name = "username")
	private String username;

	@Column(name = "numberOfPosts")
	private Integer numberOfPosts;
	
	@Column(name = "numberOfThreads")
	private Integer numberOfThreads;

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "account_id", nullable = false)
	private Account account;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL,mappedBy = "userProfile")
	private Image img;

	@OneToMany(mappedBy = "userProfile", targetEntity = Posts.class, cascade = CascadeType.ALL)
	private List<Posts> posts = new ArrayList<Posts>();
	
	@OneToMany(mappedBy = "userProfile", targetEntity = Threads.class, cascade = CascadeType.ALL)
	private List<Threads> threads = new ArrayList<Threads>();

	public UserProfile() {
	}

	public UserProfile(String username) {
		this.username = username;
		this.numberOfPosts = 0;
		this.numberOfThreads = 0;
	}

	public Long getId() { return this.id; }
	public String getUsername() { return username; }
	public Integer getNumberOfPosts() { return numberOfPosts; }
	public Integer getNumberOfThreads() { return numberOfThreads; }
	public Account getAccount() { return account; }
	public byte[] getImg() { return this.img.getPic(); }

	public void setUsername(String username) { this.username = username; }
	public void setNumberOfPosts(Integer numberOfPosts) { this.numberOfPosts = numberOfPosts; }
	public void setNumberOfThreads(Integer numberOfThreads) { this.numberOfThreads = numberOfThreads; }
	public void setAccount(Account account) { this.account = account; }
	public void setImageBytes(byte[] img) { this.img.setPic(img); }

	public void setImg(Image img) { this.img = img; }


}
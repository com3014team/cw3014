package coursework.userProfile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserProfileService {

    @Autowired
    private UserProfileRepository userProfileRepository;
    
    @PersistenceContext
	private EntityManager entityManager;

    @Transactional
    public UserProfile save(UserProfile userProfile) {
        userProfileRepository.save(userProfile);
        return userProfile;
    }
    
    
    @Transactional
	public void updateUsername(Long id, String username) {
        entityManager.joinTransaction();
		entityManager.createQuery("UPDATE UserProfile u SET u.username = :username WHERE u.account.id = :id")
			.setParameter("username", username)
			.setParameter("id", id)
			.executeUpdate();
		
		entityManager.flush();

	}

    @Transactional
    public void updateNumberOfPosts(Long id, int numberOfPosts) {
        entityManager.createQuery("UPDATE UserProfile u SET u.numberOfPosts = :numberOfPosts  WHERE u.account.id = :id")
        .setParameter("numberOfPosts", numberOfPosts)
        .setParameter("id", id)
        .executeUpdate();
        entityManager.flush();
    }
    
    @Transactional
    public void updateNumberOfThreads(Long id, int numberOfThreads) {
        entityManager.createQuery("UPDATE UserProfile u SET u.numberOfThreads = :numberOfThreads  WHERE u.account.id = :id")
        .setParameter("numberOfThreads", numberOfThreads)
        .setParameter("id", id)
        .executeUpdate();
        entityManager.flush();
    }
}

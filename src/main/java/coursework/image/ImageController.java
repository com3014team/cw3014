package coursework.image;


import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import coursework.account.AccountService;

@Controller
public class ImageController {

    @Autowired
    private AccountService accService;



    @GetMapping(value = "/getUserprofileImage")
    public void showImage(HttpServletResponse response, HttpServletRequest request, Principal principal) throws ServletException, IOException {
        byte[] img = accService.getCurrentAccount(principal.getName()).getUserProfile().getImg();
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(img);
        response.getOutputStream().close();
    }




}

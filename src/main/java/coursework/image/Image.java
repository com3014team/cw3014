package coursework.image;


import coursework.userProfile.UserProfile;

import javax.persistence.*;

@Entity
@Table(name="image")
public class Image {

    @Id
    @GeneratedValue(strategy= GenerationType.TABLE)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Lob
    @Column(name="pic")
    private byte[] pic;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_profiles_id", nullable = false)
    private UserProfile userProfile;

    public Image(){}

    public Image(String name, String type, byte[] pic){
        this.name = name;
        this.type = type;
        this.pic = pic;
    }

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getType(){
        return this.type;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public byte[] getPic() {
        return this.pic;
    }

    public void setPic(byte[] img) {
         this.pic = img;
    }

}
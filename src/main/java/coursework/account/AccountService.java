package coursework.account;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import coursework.image.Image;
import coursework.userProfile.UserProfile;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostConstruct
	protected void initialize() {
		File file = new File(System.getProperty("user.dir") + "/src/main/webapp/resources/images/defaultAvatar.jpg");
		byte[] bFile = new byte[(int) file.length()];
		try {
			FileInputStream fip = new FileInputStream(file);
			fip.read(bFile);
			fip.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Image adminImg = new Image("defaultAvatar", "jpg", bFile);

		UserProfile adminProfile = new UserProfile("adminerino");
		adminProfile.setImg(adminImg);
		adminImg.setUserProfile(adminProfile);
		Account adminAcc = new Account("admin@admin.com", "admin", "ROLE_ADMIN");
		adminAcc.setUserProfile(adminProfile);
		adminProfile.setAccount(adminAcc);
		save(adminAcc);

		Image userImg = new Image("defaultAvatar", "jpg", bFile);

		UserProfile newUserProfile = new UserProfile("userino");
		newUserProfile.setImg(userImg);
		userImg.setUserProfile(newUserProfile);
		Account newUserAcc = new Account("user@user.com", "test", "ROLE_USER");
		newUserAcc.setUserProfile(newUserProfile);
		newUserProfile.setAccount(newUserAcc);
		save(newUserAcc);

		Image img = new Image("defaultAvatar", "jpg", bFile);
		UserProfile up = new UserProfile("test");
		up.setImg(img);
		img.setUserProfile(up);
		Account acc = new Account("test@test.com", "newtest", "ROLE_USER");
		acc.setUserProfile(up);
		up.setAccount(acc);
		save(acc);

//		save(new Account("admin", "admin", "ROLE_ADMIN"));
	}

	@Transactional
	public Account save(Account account) {
		account.setPassword(passwordEncoder.encode(account.getPassword()));
		accountRepository.save(account);
		return account;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findOneByEmail(username);
		if (account == null) {
			throw new UsernameNotFoundException("user not found");
		}
		return createUser(account);
	}

	public Account getCurrentAccount(String email) {
		return accountRepository.findOneByEmail(email);
	}

	public void signin(Account account) {
		SecurityContextHolder.getContext().setAuthentication(authenticate(account));
	}

	private Authentication authenticate(Account account) {
		return new UsernamePasswordAuthenticationToken(createUser(account), null,
				Collections.singleton(createAuthority(account)));
	}

	private User createUser(Account account) {
		return new User(account.getEmail(), account.getPassword(), Collections.singleton(createAuthority(account)));
	}

	private GrantedAuthority createAuthority(Account account) {
		return new SimpleGrantedAuthority(account.getRole());
	}

//	@Transactional
//	public void updateUsername(Long id, String username) {
//		entityManager.createQuery("UPDATE Account a SET a.username = :username  WHERE a.id = :id")
//			.setParameter("username", username)
//			.setParameter("id", id)
//			.executeUpdate();
//		
//		entityManager.flush();
//
//	}


}

package coursework.threads;

import coursework.posts.Posts;
import coursework.userProfile.UserProfile;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "threads")
public class Threads implements java.io.Serializable {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@OneToMany(mappedBy = "thread", targetEntity = Posts.class, cascade = CascadeType.ALL)
	private List<Posts> posts = new ArrayList<>();

	@ManyToOne(targetEntity = UserProfile.class)
	@JoinColumn(name = "userProfile_username", referencedColumnName = "username")
	private UserProfile userProfile;

	@NotBlank(message = Threads.NOT_BLANK_MESSAGE)
	@Size(max = 100)
	private String title;

	@NotBlank(message = Threads.NOT_BLANK_MESSAGE)
	@Size(max = 255)
	private String message;

	private int upvotes;

	private int downvotes;

	private Instant created;

	private String tags;

	public Threads() {
	}

	public Threads(String title, String message, String tags) {
		this.title = title;
		this.message = message;
		this.upvotes = 0;
		this.downvotes = 0;
		this.created = Instant.now();
		this.tags = tags;
	}

	public Long getId() { return this.id; }
	public List<Posts> getPosts() { return this.posts; }
	public UserProfile getUserProfile() { return this.userProfile; }
	public String getTitle() { return title; }
	public String getMessage() { return message; }
	public int getUpvotes() { return this.upvotes; }
	public int getDownvotes() { return this.downvotes; }
	public Instant getCreated() { return this.created; }
	public String getTags() { return (this.tags); }

	public void setUserProfile(UserProfile userProfile) { this.userProfile = userProfile; }
	public void setTitle(String title) { this.title = title; }
	public void setMessage(String message) { this.message = message; }
	public void setUpvotes(int upvotes) { this.upvotes = upvotes; }
	public void setDownvotes(int downvotes) { this.downvotes = downvotes; }
	public void setTags(String tags) { this.tags = tags; }
}

package coursework.threads;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import coursework.account.Account;
import coursework.account.AccountService;
import coursework.posts.Posts;
import coursework.posts.PostsRepository;
import coursework.userProfile.UserProfileRepository;
import coursework.userProfile.UserProfileService;

@Controller
public class ThreadsController {

	@Autowired
	private PostsRepository postsRepository;

	@Autowired
	private ThreadsRepository threadsRepository;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Autowired
	private ThreadsService threadsService;
	
	@Autowired
	private AccountService accService;

	@Autowired
	private UserProfileService userProfileService;

	private static final int MAX_TITLE_LENGTH = 100;

	private static final int MAX_MESSAGE_LENGTH = 255;

	@ModelAttribute("module")
	String module() {
		return "threads";
	}

	/**
	 * Get all threads
	 */
	@GetMapping(value = "/threads")
	public String getThreads(Model model) {
		List<Threads> threads = threadsRepository.findAll();
		Map<Long, String> tags = new HashMap<Long, String>();
		for (int i = 0; i < threads.size(); i++) {
			Threads t = threads.get(i);
			tags.put(t.getId(), t.getTags());
		}
		model.addAttribute("threads", threadsService.findAllByIdAsc());
		model.addAttribute("tags", tags);

		return "threads/allThreads";
	}

	/**
	 * Get all threads that meet search criteria
	 */
	@PostMapping(value = "/threads", params = { "searchCriteria" })
	public String searchThreads(@RequestParam String searchCriteria, Model model) {
		System.out.println(searchCriteria);
		List<String> tagsEntered = Arrays.asList(searchCriteria.split(","));
		List<String> tagsToSearch = new ArrayList<String>();
		for (int i = 0; i < tagsEntered.size(); i++) {
			String t = tagsEntered.get(i).trim();
			if (t.length() != 0) {
				tagsToSearch.add(t);
			}
		}

		List<Threads> returnedThreads = threadsService.searchTags(tagsToSearch);
		Map<Long, String> returnedTags = new HashMap<Long, String>();
		for (int i = 0; i < returnedThreads.size(); i++) {
			Threads t = returnedThreads.get(i);
			returnedTags.put(t.getId(), t.getTags());
		}

		model.addAttribute("threads", returnedThreads);
		model.addAttribute("tags", returnedTags);
		return "threads/allThreads";
	}

	/**
	 * Get posts relevant to one thread
	 */
	@GetMapping(value = "/thread/get", params = { "id" })
	public String newThread(@RequestParam Long id, Model model, Principal principal) {
		Threads thread = threadsRepository.findOne(id);
		Account account = accService.getCurrentAccount(principal.getName());
		model.addAttribute("userRole", account.getRole());
		model.addAttribute("username", account.getUsername());
		model.addAttribute("thread", thread);
		model.addAttribute("tags", Arrays.asList(thread.getTags().split(",")));
		model.addAttribute("posts", postsRepository.findByThreadId(id));
		return "threads/thread";
	}

	/**
	 * Edit thread
	 */
	@GetMapping(value = "thread/edit", params = { "id" })
	public String editThread(@RequestParam Long id, Model model, Principal principal) {
		Threads thread = threadsRepository.findOne(id);
		Account account = accService.getCurrentAccount(principal.getName());
		model.addAttribute("userRole", account.getRole());
		model.addAttribute("username", account.getUsername());
		model.addAttribute("thread", thread);
		model.addAttribute("posts", postsRepository.findByThreadId(id));
		return "threads/editThread";
	}

	/**
	 * Update thread (main post)
	 */
	@PostMapping(value = "/thread/edit", params = { "id", "action=success" })
	public String updatePost(@RequestParam Long id, @Valid Threads thread, Model model) {
		threadsRepository.updateThread(thread.getTitle(), thread.getMessage(), thread.getTags(), id);

		return "redirect:/thread/get?id=" + id;
	}

	/**
	 * Redirect back to form (validation errors)
	 */
	@PostMapping(value = "/thread/edit", params = { "id", "action=warning" })
	public String returnUpdatePost(@RequestParam Long id, Threads tempThread, Model model, Principal principal) {
		Account account = accService.getCurrentAccount(principal.getName());
		Threads thread = threadsRepository.findOne(id);
		thread.setTitle(tempThread.getTitle());
		thread.setMessage(tempThread.getMessage());

		model.addAttribute("userRole", account.getRole());
		model.addAttribute("username", account.getUsername());
		model.addAttribute("thread", thread);
		model.addAttribute("posts", postsRepository.findByThreadId(id));

		if (thread.getTitle().length() > this.MAX_TITLE_LENGTH)
			model.addAttribute("message", "form.title.toolong");
		else if (thread.getMessage().length() > this.MAX_MESSAGE_LENGTH)
			model.addAttribute("message", "form.message.toolong");
		else if (thread.getTitle().length() == 0)
			model.addAttribute("message", "form.title.blank");
		else if (thread.getMessage().length() == 0)
			model.addAttribute("message", "form.message.blank");

		model.addAttribute("alertClass", "warning");

		return "threads/editThread";
	}

	/**
	 * Create new thread
	 */
	@GetMapping(value = "/threads/new")
	public String newThread(Model model) {
		model.addAttribute("thread", new Threads());
		return "threads/createThread";
	}

	/**
	 * Save new thread
	 */
	@PostMapping(value = "thread/new", params = { "action=success" })
	public String updateThread(Threads thread, Principal principal) {
		thread.setUserProfile(accService.getCurrentAccount(principal.getName()).getUserProfile());

		List<String> trimmedTags = new ArrayList<String>();
		List<String> tags = Arrays.asList(thread.getTags().split(","));
		for (int i = 0; i < tags.size(); i++) {
			String tag = tags.get(i).trim();
			if (tag.length() > 0) {
				System.out.println(tag);
				trimmedTags.add(tag);
			}
		}

		thread.setTags("".join(",", trimmedTags));

		int numberOfThreads = thread.getUserProfile().getNumberOfThreads() + 1;
		thread.getUserProfile().setNumberOfThreads(numberOfThreads);

		userProfileService.updateNumberOfThreads(accService.getCurrentAccount(principal.getName()).getId(),
				numberOfThreads);
		threadsRepository.save(thread);
		return "redirect:/thread/get?id=" + thread.getId();
	}

	/**
	 * Redirect back to form (validation errors)
	 */
	@PostMapping(value = "/thread/new", params = { "action=warning" })
	public String returnNewThread(Model model, Threads thread) {
		if (thread.getTitle().length() > this.MAX_TITLE_LENGTH)
			model.addAttribute("message", "form.title.toolong");
		else if (thread.getMessage().length() > this.MAX_MESSAGE_LENGTH)
			model.addAttribute("message", "form.message.toolong");
		else if (thread.getTitle().length() == 0)
			model.addAttribute("message", "form.title.blank");
		else if (thread.getMessage().length() == 0)
			model.addAttribute("message", "form.message.blank");

		model.addAttribute("alertClass", "warning");
		model.addAttribute("thread", thread);
		return "threads/createThread";
	}

	/**
	 * Delete a thread
	 */
	@GetMapping(value = "/thread/delete", params = { "id" })
	public String deleteThread(@RequestParam Long id) {
		Threads thread = threadsRepository.findOne(id);
		int numberOfThreads = thread.getUserProfile().getNumberOfThreads() - 1;

		userProfileService.updateNumberOfThreads(thread.getUserProfile().getAccount().getId(), numberOfThreads);

		List<Posts> postsToDelete = postsRepository.findByThreadId(id);
		Map<Long, Integer> userID_numberToDelete = new HashMap<Long, Integer>();
		for (Posts p : postsToDelete) {
			userID_numberToDelete.merge(p.getUserProfile().getAccount().getId(), 1, Integer::sum);
		}

		userID_numberToDelete.forEach((k, v) -> {
			userProfileService.updateNumberOfPosts(k, userProfileRepository.findByAccountId(k).getNumberOfPosts() - v);
		});

		threadsRepository.delete(id);
		return "redirect:/threads";
	}

	/**
	 * Upvote a thread
	 */
	@GetMapping(value = "/thread/upvote", params = { "id" })
	public String upvotePost(@RequestParam Long id) {
		System.out.println("====ID " + id);
		threadsService.upvote(id);
		return "redirect:/threads";
	}

	/**
	 * Downvote a thread
	 */
	@GetMapping(value = "/thread/downvote", params = { "id" })
	public String downvotePost(@RequestParam Long id) {
		threadsService.downvote(id);
		return "redirect:/threads";
	}
}
package coursework.threads;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import coursework.account.AccountRepository;

@Service
public class ThreadsService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ThreadsRepository threadsRepository;

	@Autowired
	private AccountRepository accountRepository;

	@PostConstruct
	protected void initialize() {
//		List<String> tags = new ArrayList<String>() {{add("Tag1"); add("Tag2");}};
//
//		Threads adminThread = new Threads("Admin Thread", "Admin?", tags);
//		Threads userThread = new Threads("User Thread", "User?", tags);
//
//		Account admin = accountRepository.findOne((long) 1);
//		Account user = accountRepository.findOne((long) 2);
//
//		adminThread.setUserProfile(admin.getUserProfile());
//		userThread.setUserProfile(user.getUserProfile());
//
//		saveThread(adminThread);
//		saveThread(userThread);
	}

	public Threads saveThread(Threads thread) {
		this.threadsRepository.save(thread);
		return thread;
	}

	public Iterable<Threads> findAll() {
		return this.threadsRepository.findAll();
	}

	public Iterable<Threads> findAllByIdAsc() {
		return this.threadsRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
	}

	/**
	 * Search database by given query tags
	 * @param tags
	 * @return list of threads containing tags sorted in ascending order of id 
	 */
	@Transactional
	public List<Threads> searchTags(List<String> tags) {
		String search = "";
		for (String t : tags) {
			search += " t.tags LIKE CONCAT( '%', '" + t + "', '%') OR";
		}

		if (search.length() == 0) // empty string given
			search = "t.id = t.id";
		else
			search = search.substring(0, search.length() - 2); // remove last or if not empty string

		return entityManager
				.createQuery("Select t from Threads t WHERE (" + search + ") ORDER BY t.id ASC", Threads.class)
				.getResultList();
	}

	@Transactional
	public void upvote(Long id) {
		entityManager.createQuery("UPDATE Threads t SET t.upvotes = t.upvotes + 1 WHERE t.id = :id")
				.setParameter("id", id).executeUpdate();
		entityManager.flush();
	}

	@Transactional
	public void downvote(Long id) {
		entityManager.createQuery("UPDATE Threads t SET t.downvotes = t.downvotes + 1 WHERE t.id = :id")
				.setParameter("id", id).executeUpdate();
		entityManager.flush();

	}

}
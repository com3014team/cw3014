package coursework.threads;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ThreadsRepository extends JpaRepository<Threads, Long>{
	
	@Transactional
	@Modifying
	@Query("UPDATE Threads SET title = :title, message = :message, tags = :tags WHERE id = :id")
	void updateThread(@Param("title") String title, @Param("message") String message, @Param("tags") String tags, @Param("id") Long id);

}
/**
 * Used to validate forms.
 */
function showSuccess() {
	document.getElementById('button-warning').style.display = 'none';
	document.getElementById('button-success').style.display = 'block';
};

function showWarning() {
	document.getElementById('button-success').style.display = 'none';
	document.getElementById('button-warning').style.display = 'block';
};

function checkBoth() {
	var t = checkTitle();
	var m = checkMessage();
	if (t && m) {
		showSuccess();
	} else {
		showWarning();
	}
};

function checkTitle() {
	var ans = false;
	var title = document.getElementById('title').value.length;

	var message = document.getElementById('message').value.length;
	var counter_title = document.getElementById('counter_title');
	
	if (title == 0 || title > 100) {
		counter_title.className = '';
		counter_title.classList.add('text-danger');
		ans = false;
	} else if (title > 0) {
		if (title == 100) {
			counter_title.className = '';
			counter_title.classList.add('text-warning');
		} else {
			counter_title.className = '';
			counter_title.classList.add('text-muted');
		}
		ans = true;
	}
	
	document.getElementById('counter_title').innerHTML = title + '/100';
	return ans;
};

function checkMessage() {
	var ans = false;
	var message = document.getElementById('message').value.length;
	var counter_message = document.getElementById('counter_message');

	if (message == 0 || message > 255) {
		counter_message.className = '';
		counter_message.classList.add('text-danger');
		ans = false;
	} else if (message > 0) {
		if (message == 255) {
			counter_message.className = '';
			counter_message.classList.add('text-warning');
		} else {
			counter_message.className = '';
			counter_message.classList.add('text-muted');
		}
		ans = true;
	}

	document.getElementById('counter_message').innerHTML = message + '/255';
	return ans;
};

function checkScreenname(){
	var username = document.getElementById('username').value.length;
	var counter_username = document.getElementById('counter_username');
	if (username > 50 || username == 0){
		counter_username.className = '';
		counter_username.classList.add('text-danger');
		showWarning();
	} else {
		if(username == 50){
			counter_username.className = '';
			counter_username.classList.add('text-warning');
		} else {
			counter_username.className = '';
			counter_username.classList.add('text-muted');
		}
		showSuccess();
	}
	document.getElementById('counter_username').innerHTML = username + '/50';
}
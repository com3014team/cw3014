//package coursework.signup;
//
//import coursework.config.WebSecurityConfigurationAware;
//
//public class SignupControllerTest extends WebSecurityConfigurationAware {
//    @Test
//    public void displaysSignupForm() throws Exception {
//        mockMvc.perform(get("/signup"))
//                .andExpect(model().attributeExists("signupForm"))
//                .andExpect(view().name("signup/signup"))
//                .andExpect(content().string(
//                        allOf(
//                                containsString("<title>Signup</title>"),
//                                containsString("<legend>Please Sign Up</legend>")
//                        ))
//                );
//        ;
//    }
//}